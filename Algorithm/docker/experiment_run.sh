#!/bin/bash

docker-compose run --rm  -e mujoco_env=FetchPickAndPlace-v1 -e log_tag=log/t1_cper  -e n_epochs=50  -e num_cpu=8 -e  her_tactile
docker-compose run --rm  -e mujoco_env=FetchPickAndPlace-v1 -e log_tag=log/t2_cper  -e n_epochs=50  -e num_cpu=8 -e  her_tactile
